#include <QPoint>
#include "CLifeController.h"

CLifeController::CLifeController(QObject* _parent) :
    QObject(_parent),
    m_model(0),
    m_width(0),
    m_height(0)
{
}

void CLifeController::resize(int _width, int _height)
{
    m_width = _width;
    m_height = _height;

    // Resize and reset
    m_model.fill(false, m_width * m_height);
    emit fieldChanged();
}

void CLifeController::toggle(int _x, int _y)
{
    if ((_x >= 0) && (_x < m_width) && (_y >= 0) && (_y < m_height))
    {
        m_model.toggleBit(m_width * _y + _x);
        emit fieldChanged();
    }
}

void CLifeController::next()
{
    if ((m_width < 3) || (m_height < 3)) {
        return;
    }

    const QBitArray prevModel(m_model);

    for (int y = 0; y < m_height; ++y)
    {
        for (int x = 0; x < m_width; ++x)
        {
            // The neighbours
            // 0 1 2
            // 3 - 4
            // 5 6 7
            QPoint neighbours[] =
            {
                {x - 1, y - 1},
                {x, y - 1},
                {x + 1, y - 1},
                {x - 1, y},
                {x + 1, y},
                {x - 1, y + 1},
                {x, y + 1},
                {x + 1, y + 1},
            };

            int counter = 0;

            for (QPoint &neighbour : neighbours)
            {
                if (neighbour.x() == -1)
                {
                    neighbour.setX(m_width - 1);
                }
                else if (neighbour.x() == m_width)
                {
                    neighbour.setX(0);
                }

                if (neighbour.y() == -1) {
                    neighbour.setY(m_height - 1);
                }
                else if (neighbour.y() == m_height)
                {
                    neighbour.setY(0);
                }

                if (prevModel[m_width * neighbour.y() + neighbour.x()])
                {
                    ++counter;
                }
            }

            int index = m_width * y + x;

            if (prevModel[index])
            {
                // The cell is alive
                if ((counter < 2) || (counter > 3))
                {
                    // Dead
                    m_model.clearBit(index);
                }
            }
            else
            {
                // The cell is dead
                if (counter == 3) {
                    // Alive
                    m_model.setBit(index);
                }
            }
        }
    }

    if (m_model == prevModel)
    {
        emit stableState();
    }
    else
    {
        emit fieldChanged();
    }
}

bool CLifeController::value(int _x, int _y) const
{
    if ((_x >= 0) && (_x < m_width) && (_y >= 0) && (_y < m_height))
    {
        return m_model[m_width * _y + _x];
    }
    else
    {
        return false;
    }
}

int CLifeController::width() const
{
    return m_width;
}

int CLifeController::height() const
{
    return m_height;
}
