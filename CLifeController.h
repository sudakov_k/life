#pragma once

#include <QObject>
#include <QBitArray>

class CLifeController : public QObject
{
    // clang-format off
    Q_OBJECT
    Q_PROPERTY(int width READ width NOTIFY fieldChanged)
    Q_PROPERTY(int height READ height NOTIFY fieldChanged)
    // clang-format on

public:
    explicit CLifeController(QObject *_parent = nullptr);

public slots:
    //!
    //! \brief resize Resize current field.
    //! \param _width The field width.
    //! \param _height The field height.
    //!
    //! The values of the cells will be reseted.
    //!
    void resize(int _width, int _height);

    //!
    //! \brief toggle Toggle the cell value.
    //! \param _x The X position.
    //! \param _y The Y position.
    //!
    void toggle(int _x, int _y);

    //!
    //! \brief next Calculates the field state on the next step.
    //!
    void next();

    //!
    //! \brief value Returns the cell value.
    //! \param _x The X position.
    //! \param _y The Y position.
    //! \return The cell value.
    //!
    bool value(int _x, int _y) const;

    //!
    //! \brief width Returns the field width.
    //! \return The field width.
    //!
    int width() const;

    //!
    //! \brief height Returns the field height.
    //! \return The field height.
    //!
    int height() const;

signals:
    //!
    //! \brief fieldChanged The field was changed.
    //!
    void fieldChanged();

    //!
    //! \brief stableState The state is not changed.
    //!
    void stableState();

private:
    QBitArray m_model;
    int m_width;
    int m_height;
};
