import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

Window {
    visible: true
    minimumWidth: 1024
    minimumHeight: 600

    ColumnLayout {
        anchors.fill: parent

        Canvas {
            Layout.fillWidth: true
            Layout.fillHeight: true

            id: canvas

            onPaint: {
                var context = canvas.getContext("2d")

                // Border style
                context.strokeStyle = "lightgray"
                context.lineWidth = 1

                // Background
                context.fillStyle = "white"
                context.fillRect(0, 0, width, height)

                if ((lifeController.width > 0) && (lifeController.height > 0)) {
                    var cellMaxWidth = width / lifeController.width
                    var cellMaxHeight = height / lifeController.height
                    var cellWidth = 0
                    var fieldOffsetX = 0
                    var fieldOffsetY = 0

                    // Square cells
                    if (cellMaxWidth >= cellMaxHeight) {
                        cellWidth = cellMaxHeight
                        fieldOffsetX = (width - cellWidth * lifeController.width) / 2
                    } else {
                        cellWidth = cellMaxWidth
                        fieldOffsetY = (height - cellWidth * lifeController.height) / 2
                    }

                    // Draw cells
                    for (var j = 0; j < lifeController.height; ++j) {
                        for (var i = 0; i < lifeController.width; ++i) {
                            // Cell
                            context.fillStyle = lifeController.value(i, j) ? "black" : "white"
                            context.fillRect(cellWidth * i + fieldOffsetX, cellWidth * j + fieldOffsetY, cellWidth, cellWidth)

                            // Cell border
                            context.strokeRect(cellWidth * i + fieldOffsetX, cellWidth * j + fieldOffsetY, cellWidth, cellWidth)
                        }
                    }
                }
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    if ((lifeController.width > 0) && (lifeController.height > 0)) {
                        var cellMaxWidth = width / lifeController.width
                        var cellMaxHeight = height / lifeController.height
                        var cellWidth = 0
                        var fieldOffsetX = 0
                        var fieldOffsetY = 0
                        var fieldEndX = width
                        var fieldEndY = height

                        // Square cells
                        if (cellMaxWidth >= cellMaxHeight) {
                            cellWidth = cellMaxHeight
                            fieldOffsetX = (width - cellWidth * lifeController.width) / 2
                            fieldEndX = fieldOffsetX + cellWidth * lifeController.width
                        } else {
                            cellWidth = cellMaxWidth
                            fieldOffsetY = (height - cellWidth * lifeController.height) / 2
                            fieldEndY = fieldOffsetY + cellWidth * lifeController.height
                        }

                        // Check the field intersection
                        if ((mouseX >= fieldOffsetX) && (mouseX < fieldEndX) && (mouseY >= fieldOffsetY) && (mouseY < fieldEndY)) {
                            var fieldX = lifeController.width * (mouseX - fieldOffsetX) / (fieldEndX - fieldOffsetX)
                            var fieldY = lifeController.height * (mouseY - fieldOffsetY) / (fieldEndY - fieldOffsetY)
                            lifeController.toggle(fieldX, fieldY)
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true

            TextField {
                id: widthEdit
                validator: fieldValidator
                placeholderText: fieldValidator.bottom + " - " + fieldValidator.top
            }

            TextField {
                id: heightEdit
                validator: fieldValidator
                placeholderText: fieldValidator.bottom + " - " + fieldValidator.top
            }

            Button {
                Layout.fillWidth: true
                text: qsTr("Resize")
                enabled: !timer.running

                onClicked: {
                    var width = parseInt(widthEdit.text, 10)
                    var height = parseInt(heightEdit.text, 10)

                    if (isNaN(width) || (width < fieldValidator.bottom)) {
                        widthEdit.text = fieldValidator.bottom
                        width = fieldValidator.bottom;
                    }

                    if (isNaN(height) || (height < fieldValidator.bottom)) {
                        heightEdit.text = fieldValidator.bottom
                        height = fieldValidator.bottom
                    }

                    lifeController.resize(width, height)
                }
            }
        }

        RowLayout {
            Button {
                Layout.fillWidth: true
                Layout.preferredWidth: 0
                text: timer.running ? qsTr("Stop") : qsTr("Play")

                onClicked: {
                    if (timer.running) {
                        timer.stop()
                    } else {
                        timer.start()
                    }
                }
            }

            Button {
                Layout.fillWidth: true
                Layout.preferredWidth: 0
                text: qsTr("Next")
                enabled: !timer.running

                onClicked: {
                    lifeController.next()
                }
            }
        }
    }

    IntValidator {
        id: fieldValidator
        bottom: 3
        top: 50
    }

    Connections {
        target: lifeController
        onFieldChanged: {
            canvas.requestPaint()
        }

        onStableState: {
            timer.stop()
        }
    }

    Timer {
        id: timer
        interval: 200
        repeat: true

        onTriggered: {
            lifeController.next()
        }
    }
}
