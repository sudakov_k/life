#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "CLifeController.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    // Properties for Qml code
    CLifeController lifeController;
    engine.rootContext()->setContextProperty("lifeController", &lifeController);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
